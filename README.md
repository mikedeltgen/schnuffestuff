schnuffestuff
=============

schnuffestuff website deploy project


Setup :
-------

You only need to set some environment variables


        # the project release tag to use
        export RELEASE_TAG=rc.latest

        # Application config
        export SCHNUFFESTUFF_DOMAIN=schnuffestuff.domain
        export SCHNUFFESTUFF_API_DOMAIN=schnuffestuff-ws.domain
        export SCHNUFFESTUFF_DB=databaseName
        export SCHNUFFESTUFF_DB_USER=dbUser
        export SCHNUFFESTUFF_DB_USER_PASSWORD=dbUserPassword
        export SCHNUFFESTUFF_EXPOSE_PORT=80
        export SCHNUFFESTUFF_DB_HOST=db_container
        export SCHNUFFESTUFF_JWT_SECRET=JWTsecret
        export SCHNUFFESTUFF_API_DOMAIN=schnuffestuff-ws.domain
        export SCHNUFFESTUFF_DOMAIN=schnuffestuff.domain
        # mail server
        export SCHNUFFESTUFF_MAIL_PASSWORD=mailPassword
        export SCHNUFFESTUFF_MAIL_USERNAME=schnuffestuff@mailserver.foo
        export SCHNUFFESTUFF_MAIL_SENDER=schnuffestuff@mailserver.foo
        export SCHNUFFESTUFF_SMTP_SERVER=mail.server.foo

        # Postgress 
        export POSTGRES_ADMIN=admin
        export POSTGRES_ADMIN_PASSWORD=adminPassword
        export PGDATA_FOLDER=/db

then spin up using :

    app@dockerHost $ docker-compose up -d

This will spin up:

- postgress database server, create the user and database specified
    by the `Application config`.

- API service will spin up as soon as DB is ready

- Front-End 